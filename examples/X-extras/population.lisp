;;;
;;; Example of always having a REPL available (using -qtpl command line option):
;;;
;;;   - run calculation in a thread
;;;   - send output to a log widget
;;;
;;; "How many years would it take to populate Earth, starting from 2 people?"
;;;

#-qt-wrapper-functions ; see README-OPTIONAL.txt
(load (in-home "src/lisp/all-wrappers"))

(defparameter *max-children*   3)
(defparameter *max-age*        65)
(defparameter *years*          2000)
(defparameter *couples*        nil)
(defparameter *to-marry*       nil)
(defparameter *matrimony*      30)
(defparameter *birth-interval* 3)
(defparameter *doubling*       nil)
(defparameter *max-doubling*   12)

;;; ui

(defvar *log-widget* (qnew "QTextBrowser"
                           "windowTitle" "Population Log"
                           "openLinks" nil
                           "size" '(500 300)))

(defun ini-ui ()
  (qrun* (qset-color *log-widget* |QPalette.Base| "lightgray")
         (|setWindowFlags| *log-widget* |Qt.WindowStaysOnTopHint|)
         (qconnect *log-widget* "anchorClicked(QUrl)" 'anchor-clicked)
         (|show| *log-widget*)))

(defun out (text)
  (qrun* (|append| *log-widget* text)))

(defun anchor-clicked (url)
  (let ((name (|toString| url)))
    (cond ((string= "one-million" name)
           (let ((*years* (expt 10 6)))
             (out "<br><h3>After one million years:</h3>")
             (qrun* (|showMaximized| *log-widget*))
             (show-result))))))

;;; calculation

(defstruct couple
  (age (cons *matrimony* *matrimony*))
  (number-children 0)
  (children nil))

(defun remove-to-marry (children)
  (let (children*)
    (dolist (child children)
      (if (< child *matrimony*)
          (push child children*)
          (push child *to-marry*)))
    (x:while (>= (length *to-marry*) 2)
      (setf *to-marry* (nreverse *to-marry*))
      (push (make-couple :age (cons (pop *to-marry*) (pop *to-marry*)))
            *couples*)
      (setf *to-marry* (nreverse *to-marry*)))
    children*))

(defun birth (couple)
  (and (< (couple-number-children couple) *max-children*)
       (zerop (mod (car (couple-age couple))
                   *birth-interval*))))

(defun %run ()
  (ini-ui)
  (let ((doubling 2)
        (old-year 1))
    (setf *couples*  nil
          *to-marry* nil
          *doubling* nil)
    (push (make-couple) *couples*)
    (dotimes (year *years*)
      (let ((pop (sum)))
        (when (>= pop (* 2 doubling))
          (push (- year old-year) *doubling*)
          (when (= *max-doubling* (length *doubling*))
            (out "<br>extrapolating...<br>")
            (show-result)
            (out "<br><i>What would happen in just <a href='one-million'>one million</a> years?</i>")
            (return-from %run))
          (setf doubling pop
                old-year year))
        (out (format nil "year: ~:D  |  population: ~:D"
                     (1+ year)
                     (sum))))
      (dolist (couple *couples*)
        (when (couple-age couple)
          (let ((a1 (car (couple-age couple)))
                (a2 (cdr (couple-age couple))))
            (setf (couple-age couple)
                  (if (and (> a1 *max-age*) (> a2 *max-age*))
                      nil
                      (cons (1+ a1) (1+ a2))))))
        (setf (couple-children couple) (mapcar '1+ (couple-children couple)))
        (when (birth couple)
          (incf (couple-number-children couple))
          (setf (couple-children couple)
                (cons 0 (couple-children couple))))
        (setf (couple-children couple)
              (remove-to-marry (couple-children couple)))))))

(defun show-result ()
  (multiple-value-bind (population year)
      (final-population)
    (out (format nil "<table border='0.5' style='font-weight: bold'>~
                    ~%<tr><td>number children</td> <td align=right>~:D</td></tr>~
                    ~%<tr><td>death</td>           <td align=right>~:D</td></tr>~
                    ~%<tr><td>doubling period</td> <td align=right style='color: red'>~:D</td></tr>~
                    ~%<tr><td>year</td>            <td align=right style='color: blue'>~:D</td></tr>~
                    ~%<tr><td>population</td>      <td align=right style='color: blue'>~:D</td></tr>~
                    ~%</table>"
                 *max-children*
                 *max-age*
                 (doubling-period)
                 year
                 population))))

(defun sum ()
  (let ((sum (length *to-marry*)))
    (dolist (couple *couples*)
      (let ((age (couple-age couple)))
        (incf sum (length (remove-if (lambda (x) (> x *max-age*))
                                     (if age
                                         (cons (car age) (cons (cdr age) (couple-children couple)))
                                         (couple-children couple)))))))
    sum))

(defun families ()
  (let ((families 0))
    (dolist (couple *couples*)
      (when (couple-age couple)
        (incf families)))
    (values families (sum))))

(defun doubling-period ()
  (let ((doubling (butlast *doubling* 4)))
    (truncate (+ 0.5 (/ (reduce '+ doubling)
                        (length doubling))))))

(defun final-population ()
  (let ((add (doubling-period)))
    (do ((year 0 (+ add year))
         (sum 2 (* 2 sum)))
        ((>= year *years*) (values sum year)))))

(defun run ()
  (mp:process-run-function :population '%run))

(run)
