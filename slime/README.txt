Please note that you need to delete "thread-safe.fas*" manually
on every upgrade of either ECL or EQL.

It will then be compiled automatically next time you use Slime.

